$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm("Deseja Excluir o Registro?", function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    
    if($("table.table-sortable").length){
        $("table.table-sortable tbody").sortable({
            update : function () {
                serial = [];
                tabela = $('table.table-sortable').attr('data-tabela');
                $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                    serial.push(elm.id.split('_')[1])
                });
                $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
            },
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            handle : $('.btn-move')
        }).disableSelection();
    }


    if($('textarea').length)
        $('textarea').ckeditor();    
    	
});	