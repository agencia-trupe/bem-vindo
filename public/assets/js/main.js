$('document').ready( function(){

	$('nav ul li a').click( function(e){
		if($(this).hasClass('ativo')){
			e.preventDefault();
		}else{
			$('nav ul li a.ativo').removeClass('ativo');
			$(this).addClass('ativo');
			$('html, body').animate({'scrollTop' : 0}, 400);
		}
	});

	if(window.location.hash){
		$("nav ul li a[href='"+window.location.hash+"']").addClass('ativo');
	}else{
		var primeiro = $('aside ul li a:first');
		primeiro.addClass('ativo');
		window.location.hash = primeiro.attr('href');
	}

	$('#link-inicio').click( function(e){
		e.preventDefault();
		var primeiro = $('aside ul li a:first');
		primeiro.addClass('ativo');
		window.location.hash = primeiro.attr('href');
	});

});