-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: boasvindas
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_03_06_142052_create_posts_table',1),('2014_03_06_142428_create_posts_etapas_table',1),('2014_03_06_142715_create_users_table',1),('2014_03_06_171806_alter_posts_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `posts_etapas_id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (6,1,'Emails @trupe.net','6-emails-trupenet','<p>O nosso servi&ccedil;o de email &eacute; provido pela locaweb para o dom&iacute;nio trupe.net, portanto todas as caixas de e-mail daqui terminam com <strong>@trupe.net.</strong></p>\r\n\r\n<p>Ao entrar na Trupe j&aacute; iremos criar sua conta (provavelmente com o seu primeiro nome) e voc&ecirc; dever&aacute; receber essa informa&ccedil;&atilde;o juntamente com uma senha inicial.</p>\r\n\r\n<p>O webmail pode ser acessado aqui :&nbsp;<a href=\"http://webmail.trupe.net/\" target=\"_blank\">http://webmail.trupe.net/</a></p>\r\n\r\n<p>Caso prefira configurar algum cliente desktop para utiliza&ccedil;&atilde;o do e-mail, os dados s&atilde;o:</p>\r\n\r\n<ul>\r\n	<li><strong>IMAP</strong>: pop.trupe.net</li>\r\n	<li><strong>POP</strong>: pop.trupe.net</li>\r\n	<li><strong>SMTP</strong>: smtp.trupe.net</li>\r\n</ul>\r\n\r\n<p>Um guia mais detalhado de instala&ccedil;&atilde;o para o Thunderbird est&aacute; dispon&iacute;vel aqui :&nbsp;<a href=\"http://wiki.locaweb.com.br/pt-br/Thunderbird\" target=\"_blank\">Configurando o Mozilla Thunderbird</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p>E os e-mails da equipe:</p>\r\n\r\n<p>Fernando - fernando@trupe.net</p>\r\n\r\n<p>Leila - leila@trupe.net</p>\r\n\r\n<p>Bruno - bruno@trupe.net</p>\r\n\r\n<p>Patrick - patrick@trupe.net</p>\r\n\r\n<p>Luiz - luiz@trupe.net</p>\r\n\r\n<p>Rafael - rafael@trupe.net</p>\r\n',2,'2014-03-07 14:54:36','2014-03-07 21:16:08'),(7,1,'Instalando o Skype','7-instalando-o-skype','',3,'2014-03-07 14:54:49','2014-03-07 14:54:49'),(8,1,'PHP, Apache, MySQL','8-php-apache-mysql','',4,'2014-03-07 14:54:59','2014-03-07 14:54:59'),(9,1,'MySQL Admin ou MySQL Workbench','9-mysql-admin-ou-mysql-workbench','',5,'2014-03-07 14:55:07','2014-03-07 14:55:07'),(10,1,'Sublime Text 2 + Package Control','10-sublime-text-2-package-control','',6,'2014-03-07 14:55:15','2014-03-07 14:55:15'),(11,1,'Navegadores','11-navegadores','',7,'2014-03-07 14:55:22','2014-03-07 14:55:22'),(12,1,'git, node, lessc, yeoman','12-git-node-lessc-yeoman','',8,'2014-03-07 14:55:34','2014-03-07 14:55:34'),(13,1,'Filezilla','13-filezilla','',9,'2014-03-07 14:55:42','2014-03-07 14:55:42'),(14,2,'Recebimento do Layout e Briefing','14-recebimento-do-layout-e-briefing','',0,'2014-03-07 14:56:14','2014-03-07 14:56:14'),(15,2,'Instalando o Laravel','15-instalando-o-laravel','',0,'2014-03-07 14:56:24','2014-03-07 14:56:24'),(16,2,'Criando o VHost','16-criando-o-vhost','',0,'2014-03-07 14:56:32','2014-03-07 14:56:32'),(17,2,'Acessando o VHost local e pela rede','17-acessando-o-vhost-local-e-pela-rede','',0,'2014-03-07 14:56:40','2014-03-07 14:56:41'),(18,2,'Criação da base de dados (Migrations)','18-criacao-da-base-de-dados-migrations','',0,'2014-03-07 14:56:54','2014-03-07 14:56:57'),(19,2,'Criação do Painel','19-criacao-do-painel','',0,'2014-03-07 14:57:02','2014-03-07 14:57:02'),(20,2,'Usando o Controle de Versão','20-usando-o-controle-de-versao','',0,'2014-03-07 14:57:12','2014-03-07 14:57:13'),(21,2,'Testes','21-testes','',0,'2014-03-07 14:57:23','2014-03-07 14:57:23'),(22,2,'Criação do Front-End','22-criacao-do-front-end','',0,'2014-03-07 14:57:33','2014-03-07 14:57:33'),(23,2,'Usando o Less','23-usando-o-less','',0,'2014-03-07 14:57:42','2014-03-07 14:57:42'),(24,2,'Observações gerais sobre Front End','24-observacoes-gerais-sobre-front-end','',0,'2014-03-07 14:57:51','2014-03-07 14:57:51'),(25,2,'Salvando o projeto no bit bucket','25-salvando-o-projeto-no-bit-bucket','',0,'2014-03-07 14:58:00','2014-03-07 14:58:00'),(26,3,'Hospedagem','26-hospedagem','',0,'2014-03-07 14:58:14','2014-03-07 14:58:14'),(27,1,'O que nós utilizamos','27-o-que-nos-utilizamos','<p>Os softwares que n&oacute;s utilizamos (e esperamos que voc&ecirc; tamb&eacute;m esteja utilizando em breve) s&atilde;o:</p>\r\n\r\n<p>Sistema Operacional: <strong>Linux Ubuntu 13.10</strong></p>\r\n\r\n<blockquote>\r\n<p>Caso voc&ecirc; n&atilde;o saiba utilizar sistemas linux, essa &eacute; a hora de aprender! :) Ter um conhecimento bom de linha de comando vai agilizar bastante o seu trabalho, e em caso de d&uacute;vidas basta perguntar.</p>\r\n</blockquote>\r\n\r\n<p>Comunica&ccedil;&atilde;o: <strong>Skype</strong></p>\r\n\r\n<blockquote>\r\n<p>Aqui o Skype &eacute; o comunicador padr&atilde;o. Crie uma conta! (caso ainda n&atilde;o possua)</p>\r\n</blockquote>\r\n\r\n<p>Servidor Web: <strong>Apache 2</strong></p>\r\n\r\n<p>Linguagem de Programa&ccedil;&atilde;o: <strong>PHP 5.3 e acima</strong></p>\r\n\r\n<p>Framework:<strong> Laravel 4.1 ou Zend</strong></p>\r\n\r\n<p>Ferramenta de Scaffold: <strong>Yeoman</strong></p>\r\n\r\n<p>Gerenciador de Depend&ecirc;ncias: <strong>Composer</strong> (para o PHP) e <strong>Bower</strong> (para o Front-End)</p>\r\n\r\n<p>Automatizador de Tarefas: <strong>Grunt</strong></p>\r\n\r\n<p>Banco de Dados: <strong>MySQL</strong></p>\r\n\r\n<p>Gerenciamento de Banco de Dados: <strong>MySQL Admin</strong> ou <strong>MySQL Workbench</strong></p>\r\n\r\n<p>Controle de Vers&atilde;o: <strong>Git</strong></p>\r\n\r\n<p>Editor de Texto: <strong>Sublime Text 2</strong></p>\r\n\r\n<p>Programa de FTP: <strong>Filezilla</strong></p>\r\n\r\n<p>N&atilde;o se assuste caso n&atilde;o conhe&ccedil;a algum ou alguns destes! Em breve voc&ecirc; estar&aacute; os utilizando trivialmente.</p>\r\n',1,'2014-03-07 15:01:27','2014-03-07 20:18:52'),(28,1,'Seja Bem-Vindo!','28-seja-bem-vindo','<p>Ol&aacute;! Seja bem-vindo &agrave; Trupe.</p>\r\n\r\n<p>Preparamos este guia para auxiliar os nossos desenvolvedores rec&eacute;m-chegados. Como n&atilde;o temos como saber o que os novos desenvolvedores sabem exatamente, vamos cobrir do mais b&aacute;sico (instala&ccedil;&atilde;o de softwares em ambiente Linux) at&eacute; a etapa final de um projeto (publica&ccedil;&atilde;o no servidor do cliente).</p>\r\n\r\n<p>A ideia &eacute; que este guia sirva n&atilde;o s&oacute; como um &quot;Tutorial&quot; para desenvolver aplica&ccedil;&otilde;es web de in&iacute;cio a fim, mas tamb&eacute;m como um guia de Refer&ecirc;ncia R&aacute;pida para os momentos em que surgirem d&uacute;vidas (&quot;<em>Como eu fa&ccedil;o para compilar estes arquivos .less mesmo?</em>&quot;).</p>\r\n\r\n<p>Vale lembrar que o conte&uacute;do aqui ainda n&atilde;o &eacute; final e est&aacute; em constante desenvolvimento.</p>\r\n\r\n<p>No presente momento a estrutura do Guia come&ccedil;a com a Instala&ccedil;&atilde;o de tudo que &eacute; necess&aacute;rio em ambiente Linux, passando pelo desenvolvimento do sistema com o Laravel 4.1 e publica&ccedil;&atilde;o. Outros itens e guias para outros sistemas operacionais podem ser adicionados em breve.&nbsp;</p>\r\n',0,'2014-03-07 19:47:42','2014-03-07 19:50:42');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_etapas`
--

DROP TABLE IF EXISTS `posts_etapas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_etapas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_etapas`
--

LOCK TABLES `posts_etapas` WRITE;
/*!40000 ALTER TABLE `posts_etapas` DISABLE KEYS */;
INSERT INTO `posts_etapas` VALUES (1,'Início','inicio',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Produção','producao',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Publicação','publicacao',3,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `posts_etapas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$uko54E2u5d6zcDQFlAj0/OHMRrfDibX7ILqwlhz1QtqNHFgoCMSxS','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-07 17:57:07
