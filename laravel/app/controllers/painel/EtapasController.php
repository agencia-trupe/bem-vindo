<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Etapa;

class EtapasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.etapas.index')->with('etapas', Etapa::orderBy('ordem', 'asc')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.etapas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Etapa;

		$object->titulo = Input::get('titulo');

		try {

			$object->save();

			$object->slug = Str::slug($object->id.'-'.Input::get('titulo'));
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Etapa criada com sucesso.');
			return Redirect::route('painel.etapas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(['Erro ao criar Etapa!']);	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.etapas.edit')->with('etapa', Etapa::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Etapa::find($id);

		$object->titulo = Input::get('titulo');
		
		try {

			$object->save();

			$object->slug = Str::slug($object->id.'-'.Input::get('titulo'));
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Etapa alterada com sucesso.');
			return Redirect::route('painel.etapas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(['Erro ao criar Etapa!']);	

		}		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Etapa::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Etapa removida com sucesso.');

		return Redirect::route('painel.etapas.index');
	}

}