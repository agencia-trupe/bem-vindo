<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Texto, Etapa;

class TextosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::get('etapa')){
			$filtroEtapa = Input::get('etapa');
			$posts = Etapa::find($filtroEtapa)->posts()->orderBy('ordem', 'asc')->get();
		}else{
			$filtroEtapa = false;
			$posts = false;
		}

		$this->layout->content = View::make('backend.textos.index')->with('textos', $posts)
																	->with('etapas', Etapa::orderBy('ordem', 'asc')->get())
																	->with('filtroEtapa', $filtroEtapa);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.textos.form')->with('etapas', Etapa::orderBy('ordem', 'asc')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Texto;

		$object->posts_etapas_id = Input::get('posts_etapas_id');
		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		
		if(!Input::get('posts_etapas_id') || !Input::get('titulo')){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(['Erro ao criar Texto! Selecione uma Etapa e um Título']);
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'-'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Texto criado com sucesso.');
				return Redirect::route('painel.textos.index', array('etapa' => $object->posts_etapas_id));

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(['Erro ao criar Texto!<br>'.$e->getMessage()]);	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.textos.edit')->with('registro', Texto::find($id))->with('etapas', Etapa::orderBy('ordem', 'asc')->get());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Texto::find($id);

		$object->posts_etapas_id = Input::get('posts_etapas_id');
		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->slug = Str::slug($id.'-'.Input::get('titulo'));
		
		if(!Input::get('posts_etapas_id') || !Input::get('titulo')){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(['Erro ao criar Texto! Selecione uma Etapa e um Título']);
		}else{
			try {

				$object->save();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Texto alterado com sucesso.');
				return Redirect::route('painel.textos.index', array('etapa' => $object->posts_etapas_id));

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(['Erro ao criar Texto!<br>'.$e->getMessage()]);	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Texto::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::back();
	}

}