<?php

use Etapa;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('etapas', Etapa::orderBy('ordem', 'asc')->get());

		$this->layout->content = View::make('frontend.home')->with('etapas', Etapa::orderBy('ordem', 'asc')->get());
	}

}