<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'username' => 'trupe',
            	'email' => 'contato@trupe.net',
            	'password' => Hash::make('senhatrupe')
            ]
        ];

        DB::table('users')->insert($data);
    }

}