<?php

class PostsEtapasTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'titulo' => 'Início',
				'slug' => 'inicio',
				'ordem' => '0',
            ],
            [
            	'titulo' => 'Produção',
				'slug' => 'producao',
				'ordem' => '1',
            ],
            [
            	'titulo' => 'Publicação',
				'slug' => 'publicacao',
				'ordem' => '2',
            ]
        ];

        DB::table('posts_etapas')->insert($data);
    }

}