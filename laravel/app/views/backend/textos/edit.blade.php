@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.textos.update', $registro->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inpuEtapa">Etapa</label>
					<select name="posts_etapas_id" id="inpuEtapa" required class="form-control">
						<option value="">--</option>
						@if($etapas)
							@foreach($etapas as $etapa)
								<option value="{{$etapa->id}}" @if($registro->posts_etapas_id == $etapa->id) selected @endif>{{$etapa->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

		    	<div class="form-group">
					<label for="inputTitulo">Título</label>
					<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputTexto">Texto</label>
				<textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.textos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop