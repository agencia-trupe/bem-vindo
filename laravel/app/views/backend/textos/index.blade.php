@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<h2>
  		Textos <a href="{{ URL::route('painel.textos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Texto</a>
	</h2>

	@if($etapas)
		<br>
		Selecione uma Etapa:<br>
		<div class="btn-group">
			<?php foreach ($etapas as $key => $value): ?>
				<a href="painel/textos?etapa={{$value->id}}" title="{{$value->titulo}}" class="btn btn-sm @if(isset($filtroEtapa) && $filtroEtapa == $value->id) btn-warning @else btn-default @endif ">{{$value->titulo}}</a>
			<?php endforeach ?>
		</div>
	@endif

	<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="posts">
        
        <thead>
    		<tr>
          		<th>Ordenar</th>
          		<th>Título</th>
          		<th>Texto</th>
      			<th><span class="glyphicon glyphicon-cog"></span></th>
    		</tr>
  		</thead>

  		<tbody>
  		@if($textos && count($textos))
	    	@foreach ($textos as $registro)

	        	<tr class="tr-row" id="row_{{ $registro->id }}">
	              	<td class="move-actions"><a href="#" class="btn btn-info btn-move btn-sm">mover</a></td>
	              	<td>{{$registro->titulo}}</td>
	              	<td>{{Str::words(strip_tags($registro->texto), 15)}}</td>
	          		<td class="crud-actions">
	            		<a href="{{ URL::route('painel.textos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	            	   	{{ Form::open(array('route' => array('painel.textos.destroy', $registro->id), 'method' => 'delete')) }}
	                    	<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                   	{{ Form::close() }}
	          		</td>
	        	</tr>

	    	@endforeach
    	@else
			<tr>
				<td colspan="4">
					<h3>
						@if($filtroEtapa)
							Nenhum Texto Encontrado para a Etapa
						@else
							Selecione uma Etapa
						@endif
					</h3>
				</td>
			</tr>
    	@endif
  		</tbody>

    </table>
    
</div>

@stop