@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.textos.store')}}" method="post">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inpuEtapa">Etapa</label>
					<select name="posts_etapas_id" id="inpuEtapa" required class="form-control">
						<option value="">--</option>
						@if($etapas)
							@foreach($etapas as $etapa)
								<option value="{{$etapa->id}}" @if(Session::has('formulario') && Session::get('formulario')['posts_etapas_id'] == $etapa->id) selected @endif>{{$etapa->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

		    	<div class="form-group">
					<label for="inputTitulo">Título</label>
					<input type="text" class="form-control" id="inputTitulo" name="titulo"  @if(Session::has('formulario')) value="{{ Session::get('formulario')['titulo'] }}" @endif required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputTexto">Texto</label>
				<textarea name="texto" class="form-control" id="inputTexto"> @if(Session::has('formulario')) {{ Session::get('formulario')['texto'] }} @endif</textarea>
			</div>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.textos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			
		</form>
    </div>
    
@stop