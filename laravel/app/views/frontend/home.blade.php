@section('conteudo')

    @if($etapas)
        @foreach($etapas as $etapa)
            @if($etapa->posts())
                @foreach($etapa->posts()->orderBy('ordem', 'asc')->get() as $texto)
                    <li><a href="#{{$texto->slug}}" title="{{$texto->titulo}}">{{$texto->titulo}}</a></li>
                    <article id="{{$texto->slug}}">
                        <h1>{{$texto->titulo}}</h1>

                        {{$texto->texto}}
                    </article>
                @endforeach                
            @endif
        @endforeach
    @endif
    
@stop