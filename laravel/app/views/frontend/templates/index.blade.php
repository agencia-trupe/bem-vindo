<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bem-Vindo!</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->
        <?=Assets::CSS(array(
            'vendor/normalize-css/normalize',
            'css/main',
            'css/base'
        ))?>

        <?=Assets::JS(array(
            'vendor/modernizr/modernizr',
            'vendor/less.js/dist/less-1.6.2.min'
        ))?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="bg"></div>
        <aside>
            
            <a href="#" title="Início" id="link-inicio">
                <img src="http://trupe.net/imagens/logo_trupe.png" alt="Trupe Agência Criativa">
            </a>

            <nav>
                @if($etapas)
                    @foreach($etapas as $etapa)
                        <h1>{{$etapa->titulo}}</h1>
                        @if($etapa->posts())
                            <ul>
                            @foreach($etapa->posts()->orderBy('ordem', 'asc')->get() as $texto)
                                <li><a href="#{{$texto->slug}}" title="{{$texto->titulo}}">{{$texto->titulo}}</a></li>
                            @endforeach
                            </ul>
                        @endif
                    @endforeach
                @endif                
            </nav>

        </aside>

        <section>

            @yield('conteudo')

        </section>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        
        <?=Assets::JS(array(
            'js/main'
        ))?>
        
    </body>
</html>
